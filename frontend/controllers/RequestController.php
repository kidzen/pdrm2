<?php

namespace frontend\controllers;

use Yii;
use common\models\AsetInstOrder;
use common\models\AsetInstMaintenance;
use common\models\AsetInventoryItem;
use common\models\search\AsetInstOrderSearch;
use common\models\search\AsetInventoryItemSearch;
use common\models\search\AsetInstMaintenanceSearch;
use common\models\AssetOrderList;
use common\models\search\AssetOrderListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class RequestController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['post'],
                    ],
                ],
                'access' => [
                    'class' => \yii\filters\AccessControl::className(),
                    'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['purchasing','disposal','dispose','approval','maintenance','received','reject', 'add-aset-inst-order-item'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true
                    ]
                ]
            ]
        ];
    }

    public function actionAssetRegistration()
    {
        $model = new AsetInstOrder();
        $model->inst_no = $model->generateInstNo();
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['asset-approval']);
            // return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('asset-registration', [
                'model' => $model,
                ]);
        // return $this->render('asset-registration');
    }
    public function actionPurchasing()
    {
        $model = new AsetInstOrder();
        $model->inst_no = $model->generateInstNo();
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['approval']);
            // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('check-in', [
                'model' => $model,
                ]);
        }
        return $this->render('check-in');
    }

    public function actionApproval()
    {
        $searchModel = new AssetOrderListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('asset-order-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }

    public function actionDisposal($id = null)
    {
        if($id != null){

        } else {
            return $this->redirect(['maintenance']);
        }
    }
    public function actionDispose($id)
    {
        $model = InventoryItem::dispose($id);
            return $this->redirect(['maintenance']);
    }

    public function actionMaintenance($id = null)
    {
        if($id != null){
            $model = new InstMaintenance();
            $model->inst_no = $model->generateInstNo();
            $model->item_id = $id;

            if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
                $searchModel = new InstMaintenanceSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('maintenance-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    ]);
            } else {
                return $this->render('maintenance', [
                    'model' => $model,
                    ]);

            }

        } else {
            $searchModel = new InventoryItemSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('inventory-item/index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                ]);

        }

    }

    public function actionReceived($id)
    {
        AssetOrderList::received($id);
        return $this->redirect(['approval']);
    }

    public function actionReject($id)
    {
        $model = AssetOrderList::reject($id);
        return $this->redirect(['approval']);
    }

    public function actionAddAsetInstOrderItem()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('AsetInstOrderItem');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formAsetInstOrderItem', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
