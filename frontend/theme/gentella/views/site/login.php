<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


        <div class="animate form login_form">
          <section class="login_content">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
              <h1>Login</h1>
              <div>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder'=>'Username'])->label(false) ?>
              </div>
              <div>
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Username'])->label(false) ?>
              </div>
              <div>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
              </div>
              <div>
                <?= Html::submitButton('Log in', ['class' => 'btn btn-default submit', 'name' => 'login-button']) ?>
                  <?= Html::a('Lost your password?',['/site/request-password-reset'],['reset_pass']) ?>
                <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <?= Html::a(' Create Account ',['/site/signup'],['to_register']) ?>
                  <!-- <a href="#signup" class="to_register"> Create Account </a> -->
                </p>

                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-paw"></i> Sarra Global Sdn Bhd</h1>
                  <p>©2016 All Rights Reserved. Sarra Global Sdn Bhd. Privacy and Terms</p>
                </div>

              </div>
            <?php ActiveForm::end(); ?>
          </section>
        </div>

