<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


        <div id="register" class="animate form login_form">
        <!-- <div id="register" class="animate form registration_form"> -->
          <section class="login_content">
            <?php $form = ActiveForm::begin(['id' => 'register-form']); ?>
              <h1>Create Account</h1>
              <div>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder'=>'Username'])->label(false) ?>
              </div>
              <div>
                <?= $form->field($model, 'email')->textInput(['placeholder'=>'Email'])->label(false) ?>
              </div>
              <div>
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false) ?>
              </div>
              <div>
                <?= Html::submitButton('Submit', ['class' => 'btn btn-default submit', 'name' => 'signup-button']) ?>
                <!-- <a class="btn btn-default submit" href="index.html">Submit</a> -->
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <!-- <p class="change_link">Already a member ? -->
                <?= Html::a(' Log in ',['/site/login'],['to_register']) ?>
                  <!-- <a href="#signin" class="to_register"> Log in </a> -->
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Sarra Global Sdn Bhd</h1>
                  <p>©2016 All Rights Reserved. Sarra Global Sdn Bhd. Privacy and Terms</p>
                </div>
              </div>
            <?php ActiveForm::end(); ?>
          </section>
        </div>

