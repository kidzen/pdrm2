<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AssetOrderListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Asset Order List';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="asset-order-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_searchReceivedOrderList', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute'=>'inst_no',
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true,
            'groupedRow' => true, // move grouped column to a single grouped row
            'groupOddCssClass' => 'kv-grouped-row', // configure odd group cell css class
            'groupEvenCssClass' => 'kv-grouped-row',
        ],
        [
            'attribute'=>'category',
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
        [
            'attribute'=>'inventory',
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
        [
            'attribute'=>'quantity_request',
            'hAlign' => 'center', 'vAlign' => 'middle',
            // 'group' => true, 'subGroupOf' => 2,
        ],
        [
            'attribute'=>'quantity_received',
            'hAlign' => 'center', 'vAlign' => 'middle',
            // 'group' => true, 'subGroupOf' => 2,
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{received}{reject}',
            // 'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
            // 'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
            'buttons' => [
                'received' => function ($url, $model) {
                    if ($model->quantity_request !== $model->quantity_received) {
                        return Html::a('<span class="glyphicon glyphicon-check"></span>', ['received', 'id' => $model->id], ['title' => Yii::t('yii', 'Received'), 'data-toggle' => 'tooltip','data-method' => 'POST']);
                    }
                },
                        'reject' => function ($url, $model) {
//                        if ($model->DELETED === 0) {
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('yii', 'Rejected'), 'data-toggle' => 'tooltip',
                                'data-method' => 'post']);
                       // }
                }
                ],
            ],
        // ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-asset-order-list']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
