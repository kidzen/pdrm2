<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AssetOrderListSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-asset-order-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'inst_no')->textInput(['maxlength' => true, 'placeholder' => 'Inst No']) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true, 'placeholder' => 'Category']) ?>

    <?= $form->field($model, 'inventory')->textInput(['maxlength' => true, 'placeholder' => 'Inventory']) ?>

    <?= $form->field($model, 'quantity_request')->textInput(['placeholder' => 'Quantity Request']) ?>

    <?php /* echo $form->field($model, 'quantity_received')->textInput(['placeholder' => 'Quantity Received']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
