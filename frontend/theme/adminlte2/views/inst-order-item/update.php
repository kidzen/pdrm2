<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InstOrderItem */

$this->title = 'Update Inst Order Item: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inst Order Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inst-order-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
