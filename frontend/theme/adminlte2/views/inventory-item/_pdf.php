<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InventoryItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inventory Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inventory Item'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'inventory.name',
                'label' => 'Inventory'
            ],
        'serial_no',
        'type',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerInstMaintenance->totalCount){
    $gridColumnInstMaintenance = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 't.id',
                'label' => 'T'
            ],
        'inst_no',
        [
                'attribute' => 'vendor.name',
                'label' => 'Vendor'
            ],
                'type',
        'total_cost',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInstMaintenance,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Instruction Maintenance'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnInstMaintenance
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerItemMovement->totalCount){
    $gridColumnItemMovement = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'reff_id',
        'reff_no',
        'type',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerItemMovement,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Item Movement'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnItemMovement
    ]);
}
?>
    </div>
</div>
