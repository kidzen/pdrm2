<div class="form-group" id="add-inst-maintenance">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'InstMaintenance',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        't_id' => [
            'label' => 'Transaction',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\Transaction::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Transaction'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'inst_no' => ['type' => TabularForm::INPUT_TEXT],
        'vendor_id' => [
            'label' => 'Vendor',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\Vendor::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vendor'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'type' => ['type' => TabularForm::INPUT_TEXT],
        'total_cost' => ['type' => TabularForm::INPUT_TEXT],
        'approval' => ['type' => TabularForm::INPUT_TEXT],
        'approved_by' => ['type' => TabularForm::INPUT_TEXT],
        'approved_at' => ['type' => TabularForm::INPUT_TEXT],
        'received_by' => ['type' => TabularForm::INPUT_TEXT],
        'received_at' => ['type' => TabularForm::INPUT_TEXT],
        'requested_by' => ['type' => TabularForm::INPUT_TEXT],
        'requested_at' => ['type' => TabularForm::INPUT_TEXT],
        'remark' => ['type' => TabularForm::INPUT_TEXT],
        'status' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowInstMaintenance(' . $key . '); return false;', 'id' => 'inst-maintenance-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Instruction Maintenance', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowInstMaintenance()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

