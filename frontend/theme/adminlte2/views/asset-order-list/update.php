<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AssetOrderList */

$this->title = 'Update Asset Order List: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Order List', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-order-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
