<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AssetOrderList */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Order List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-order-list-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Asset Order List'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'inst_no',
        'category',
        'inventory',
        'quantity_request',
        'quantity_received',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
