<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Vendor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Vendor'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
<?=
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF',
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'address',
        'phone',
        'fax',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerInstMaintenance->totalCount){
    $gridColumnInstMaintenance = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 't.id',
                'label' => 'T'
            ],
            'inst_no',
                        [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
            'type',
            'total_cost',
            'approval',
            'approved_by',
            'approved_at',
            'received_by',
            'received_at',
            'requested_by',
            'requested_at',
            'remark',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInstMaintenance,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inst-maintenance']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Instruction Maintenance'),
        ],
        'columns' => $gridColumnInstMaintenance
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerInstOrder->totalCount){
    $gridColumnInstOrder = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 't.id',
                'label' => 'T'
            ],
            'inst_no',
                        'type',
            'approval',
            'approved_by',
            'approved_at',
            'received_by',
            'received_at',
            'requested_by',
            'requested_at',
            'remark',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInstOrder,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inst-order']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inst Order'),
        ],
        'columns' => $gridColumnInstOrder
    ]);
}
?>
    </div>
</div>
