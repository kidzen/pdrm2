<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InstOrder */

$this->title = 'Create Inst Order';
$this->params['breadcrumbs'][] = ['label' => 'Inst Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inst-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
