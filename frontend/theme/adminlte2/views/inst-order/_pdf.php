<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InstOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inst Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inst-order-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inst Order'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 't.id',
                'label' => 'T'
            ],
        'inst_no',
        [
                'attribute' => 'vendor.name',
                'label' => 'Vendor'
            ],
        'type',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInstOrderItem->totalCount){
    $gridColumnInstOrderItem = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                [
                'attribute' => 'inventory.name',
                'label' => 'Inventory'
            ],
        [
                'attribute' => 'itemMovement.id',
                'label' => 'Item Movement'
            ],
        'received_date',
        'quantity_request',
        'quantity_received',
        'remark',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInstOrderItem,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Inst Order Item'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnInstOrderItem
    ]);
}
?>
    </div>
</div>
