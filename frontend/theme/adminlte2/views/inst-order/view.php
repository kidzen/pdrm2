<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InstOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inst Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inst-order-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inst Order'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 't.id',
            'label' => 'T',
        ],
        'inst_no',
        [
            'attribute' => 'vendor.name',
            'label' => 'Vendor',
        ],
        'type',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInstOrderItem->totalCount){
    $gridColumnInstOrderItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'inventory.name',
                'label' => 'Inventory'
            ],
            [
                'attribute' => 'itemMovement.id',
                'label' => 'Item Movement'
            ],
            'received_date',
            'quantity_request',
            'quantity_received',
            'remark',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInstOrderItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inst-order-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inst Order Item'),
        ],
        'columns' => $gridColumnInstOrderItem
    ]);
}
?>
    </div>
</div>
