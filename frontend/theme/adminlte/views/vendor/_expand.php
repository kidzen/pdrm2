<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Vendor'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Aset Inst Maintenance'),
        'content' => $this->render('_dataAsetInstMaintenance', [
            'model' => $model,
            'row' => $model->asetInstMaintenances,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Aset Inst Order'),
        'content' => $this->render('_dataAsetInstOrder', [
            'model' => $model,
            'row' => $model->asetInstOrders,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Aset Warranty'),
        'content' => $this->render('_dataAsetWarranty', [
            'model' => $model,
            'row' => $model->asetWarranties,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
