<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Vendor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Vendor'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'address',
        'phone',
        'fax',
        'email:email',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetInstMaintenance->totalCount){
    $gridColumnAsetInstMaintenance = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 't.id',
                'label' => 'T'
            ],
            'inst_no',
                        [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
            'type',
            'total_cost',
            'approval',
            'approved_by',
            'received_by',
            'requested_by',
            'requested_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetInstMaintenance,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-inst-maintenance']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Inst Maintenance'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetInstMaintenance
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetInstOrder->totalCount){
    $gridColumnAsetInstOrder = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 't.id',
                'label' => 'T'
            ],
            'inst_no',
                        'type',
            'approval',
            'approved_by',
            'received_by',
            'requested_by',
            'requested_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetInstOrder,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-inst-order']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Inst Order'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetInstOrder
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetWarranty->totalCount){
    $gridColumnAsetWarranty = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'start_date',
            'end_date',
            'type',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetWarranty,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-warranty']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Warranty'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetWarranty
    ]);
}
?>
    </div>
</div>
