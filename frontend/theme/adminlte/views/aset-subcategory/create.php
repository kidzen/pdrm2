<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetSubcategory */

$this->title = 'Create Aset Subcategory';
$this->params['breadcrumbs'][] = ['label' => 'Aset Subcategory', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-subcategory-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
