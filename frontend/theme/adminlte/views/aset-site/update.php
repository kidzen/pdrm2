<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AsetSite */

$this->title = 'Update Aset Site: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Aset Site', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aset-site-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
