<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetSite */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Aset Site', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-site-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Aset Site'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetItemPlacement->totalCount){
    $gridColumnAsetItemPlacement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
            [
                'attribute' => 'itemMovement.id',
                'label' => 'Item Movement'
            ],
                        'type',
            'approval',
            'approved_by',
            'received_by',
            'requested_by',
            'requested_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetItemPlacement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-item-placement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Item Placement'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetItemPlacement
    ]);
}
?>
    </div>
</div>
