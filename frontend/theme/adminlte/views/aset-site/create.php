<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetSite */

$this->title = 'Create Aset Site';
$this->params['breadcrumbs'][] = ['label' => 'Aset Site', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-site-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
