<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AsetInstOrderItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-aset-inst-order-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'inst_order_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\AsetInstOrder::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Aset inst order'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'inventory_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\AsetInventory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Aset inventory'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'delivery_no')->textInput(['maxlength' => true, 'placeholder' => 'Delivery No']) ?>

    <?= $form->field($model, 'received_date')->textInput(['maxlength' => true, 'placeholder' => 'Received Date']) ?>

    <?php /* echo $form->field($model, 'quantity_request')->textInput(['placeholder' => 'Quantity Request']) */ ?>

    <?php /* echo $form->field($model, 'quantity_received')->textInput(['placeholder' => 'Quantity Received']) */ ?>

    <?php /* echo $form->field($model, 'price_per_item')->textInput(['placeholder' => 'Price Per Item']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
