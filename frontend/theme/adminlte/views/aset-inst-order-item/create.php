<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetInstOrderItem */

$this->title = 'Create Aset Inst Order Item';
$this->params['breadcrumbs'][] = ['label' => 'Aset Inst Order Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-inst-order-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
