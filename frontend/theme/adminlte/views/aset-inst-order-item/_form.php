<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AsetInstOrderItem */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="aset-inst-order-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'inst_order_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\AsetInstOrder::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Aset inst order'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'inventory_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\AsetInventory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Aset inventory'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'delivery_no')->textInput(['maxlength' => true, 'placeholder' => 'Delivery No']) ?>

    <?= $form->field($model, 'received_date')->textInput(['maxlength' => true, 'placeholder' => 'Received Date']) ?>

    <?= $form->field($model, 'quantity_request')->textInput(['placeholder' => 'Quantity Request']) ?>

    <?= $form->field($model, 'quantity_received')->textInput(['placeholder' => 'Quantity Received']) ?>

    <?= $form->field($model, 'price_per_item')->textInput(['placeholder' => 'Price Per Item']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
