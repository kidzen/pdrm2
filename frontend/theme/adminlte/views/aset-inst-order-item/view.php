<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetInstOrderItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Inst Order Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-inst-order-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Aset Inst Order Item'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'instOrder.id',
            'label' => 'Inst Order',
        ],
        [
            'attribute' => 'inventory.name',
            'label' => 'Inventory',
        ],
        'delivery_no',
        'received_date',
        'quantity_request',
        'quantity_received',
        'price_per_item',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
