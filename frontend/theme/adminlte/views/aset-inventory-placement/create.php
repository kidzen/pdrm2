<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetInventoryPlacement */

$this->title = 'Create Aset Inventory Placement';
$this->params['breadcrumbs'][] = ['label' => 'Aset Inventory Placement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-inventory-placement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
