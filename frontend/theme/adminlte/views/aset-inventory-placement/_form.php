<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AsetInventoryPlacement */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="aset-inventory-placement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'asset_id')->textInput(['placeholder' => 'Asset']) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true, 'placeholder' => 'Location']) ?>

    <?= $form->field($model, 'approval')->textInput(['placeholder' => 'Approval']) ?>

    <?= $form->field($model, 'approved_by')->textInput(['placeholder' => 'Approved By']) ?>

    <?= $form->field($model, 'received_by')->textInput(['placeholder' => 'Received By']) ?>

    <?= $form->field($model, 'requested_by')->textInput(['placeholder' => 'Requested By']) ?>

    <?= $form->field($model, 'requested_at')->textInput(['placeholder' => 'Requested At']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
