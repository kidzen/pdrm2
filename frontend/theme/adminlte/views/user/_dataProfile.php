<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model Profile */

?>
<?php if(!is_null($model)): ?>
<div>

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
    <?php 
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            'user_id',
            'name',
            'position',
            'department',
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
    ?>
    </div>
</div>
<?php else: ?>
<div class="profile-view">
    <div class="row">
        <div class="col-sm-9">
            <h2>Profile</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">Profile not set.</div>
    </div>
</div>
<?php endif; ?>
