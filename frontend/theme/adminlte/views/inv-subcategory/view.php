<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InvSubcategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Inv Subcategory', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-subcategory-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inv Subcategory'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'category.name',
            'label' => 'Category',
        ],
        'name',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInvInventory->totalCount){
    $gridColumnInvInventory = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'card_no',
            'code_no',
            'description',
            'quantity',
            'min_stock',
            'location',
            'approved',
            'approved_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInvInventory,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-inventory']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Inventory'),
        ],
        'export' => false,
        'columns' => $gridColumnInvInventory
    ]);
}
?>
    </div>
</div>
