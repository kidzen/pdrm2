<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AssetOrderListSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-asset-order-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'inventory_id')->textInput(['placeholder' => 'Inventory']) ?>

    <?= $form->field($model, 'inst_order_id')->textInput(['placeholder' => 'Inst Order']) ?>

    <?= $form->field($model, 'category_id')->textInput(['placeholder' => 'Category']) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true, 'placeholder' => 'Category']) ?>

    <?php /* echo $form->field($model, 'subcategory')->textInput(['maxlength' => true, 'placeholder' => 'Subcategory']) */ ?>

    <?php /* echo $form->field($model, 'inventory')->textInput(['maxlength' => true, 'placeholder' => 'Inventory']) */ ?>

    <?php /* echo $form->field($model, 'inst_no')->textInput(['maxlength' => true, 'placeholder' => 'Inst No']) */ ?>

    <?php /* echo $form->field($model, 'quantity_request')->textInput(['placeholder' => 'Quantity Request']) */ ?>

    <?php /* echo $form->field($model, 'quantity_received')->textInput(['placeholder' => 'Quantity Received']) */ ?>

    <?php /* echo $form->field($model, 'price_per_item')->textInput(['placeholder' => 'Price Per Item']) */ ?>

    <?php /* echo $form->field($model, 'io_status')->textInput(['placeholder' => 'Io Status']) */ ?>

    <?php /* echo $form->field($model, 'ioi_status')->textInput(['placeholder' => 'Ioi Status']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
