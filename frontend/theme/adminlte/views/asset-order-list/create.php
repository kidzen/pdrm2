<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AssetOrderList */

$this->title = 'Create Asset Order List';
$this->params['breadcrumbs'][] = ['label' => 'Asset Order List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-order-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
