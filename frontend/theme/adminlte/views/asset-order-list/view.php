<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AssetOrderList */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Order List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-order-list-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Asset Order List'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', ], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', ], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'inventory_id',
        'inst_order_id',
        'category_id',
        'category',
        'subcategory',
        'inventory',
        'inst_no',
        'quantity_request',
        'quantity_received',
        'price_per_item',
        'io_status',
        'ioi_status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
