<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
<!--         <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
 -->
        <div class="user-panel">
            <div style="text-align: center">
                <!-- <img src="<?= Yii::getAlias('@webroot') ?>/pdrm/logo-pdrm.jpeg" style="width: 200px" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/> -->
                <img src="http://localhost<?= Yii::$app->request->baseUrl ?>/pdrm/logo-pdrm.png" style="width: 200px" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/>
                <!--<img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" style="width: 200px" alt="TUDM"/>-->
            </div>
        </div>
        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
 -->        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Request', 'icon' => 'database',
                        'items' => [
                            ['label' => 'Asset Registration', 'icon' => 'user', 'url' => ['/request/asset-registration']],
                            ['label' => 'Inventory Check In', 'icon' => 'user', 'url' => ['/request/inventory-checkin']],
                            ['label' => 'Asset Maintenance', 'icon' => 'user', 'url' => ['/request/maintenance']],
                            [
                                'label' => 'Disposal', 'icon' => 'database',
                                'items' => [
                                    ['label' => 'Asset Disposal', 'icon' => 'user', 'url' => ['/request/asset-disposal']],
                                    ['label' => 'Inventory Disposal', 'icon' => 'user', 'url' => ['/request/inventory-disposal']],
                                ]
                            ],
                        ]
                    ],
                    [
                        'label' => 'Approval', 'icon' => 'database',
                        'items' => [
                            ['label' => 'Asset Request', 'icon' => 'user', 'url' => ['/request/asset-approval']],
                            ['label' => 'Inventory Checkout Request', 'icon' => 'user', 'url' => ['/request/inventory-checkout-approval']],
                            ['label' => 'Asset Disposal Request', 'icon' => 'user', 'url' => ['/request/asset-disposal']],
                            ['label' => 'Asset Maintenance Request', 'icon' => 'user', 'url' => ['/request/asset-maintenance']],
                        ]
                    ],
                    [
                        'label' => 'Assets Record', 'icon' => 'database',
                        'items' => [
                            ['label' => 'Order Instruction', 'icon' => 'user', 'url' => ['/inst-order/index']],
                            ['label' => 'Maintenance Instruction', 'icon' => 'user', 'url' => ['/inst-maintenance/index']],
                            ['label' => 'Category', 'icon' => 'user', 'url' => ['/category/index']],
                            ['label' => 'Inventory', 'icon' => 'user', 'url' => ['/inventory/index']],
                            ['label' => 'Inventory Item', 'icon' => 'user', 'url' => ['/inventory-item/index']],
                        ]
                    ],
                    [
                        'label' => 'Analysis', 'icon' => 'database',
                        'items' => [
                            ['label' => 'Yearly Report', 'icon' => 'user', 'url' => ['/report-year/index']],
                            ['label' => 'Transaction List', 'icon' => 'user', 'url' => ['/transaction/index']],
                            ['label' => 'Item Movement History', 'icon' => 'user', 'url' => ['/item-movement/index']],
                        ]
                    ],
                    [
                        'label' => 'Administration', 'icon' => 'database',
                        'items' => [
                            ['label' => 'User', 'icon' => 'user', 'url' => ['/user/index']],
                            ['label' => 'Profile', 'icon' => 'user', 'url' => ['/profile/index']],
                            ['label' => 'Role', 'icon' => 'user', 'url' => ['/role/index']],
                            ['label' => 'Vendor', 'icon' => 'user', 'url' => ['/vendor/index']],
                            ['label' => 'Switch Theme', 'icon' => 'user', 'url' => ['/site/switch-theme']],
                        ]
                    ],
                    [
                        // 'visible'=>false,
                        'label' => 'Dev tools',
                        'icon' => 'share',
                        // 'url' => '#',
                        'items' => [
                            [
                                'label' => 'Database',
                                'icon' => 'database',
                                'items' => [
                                        ['label' => 'User', 'icon' => 'user', 'url' => ['/user/index']],
                                        ['label' => 'Profile', 'icon' => 'user', 'url' => ['/profile/index']],
                                        ['label' => 'Role', 'icon' => 'user', 'url' => ['/role/index']],
                                        ['label' => 'Vendor', 'icon' => 'user', 'url' => ['/vendor/index']],

                                        ['label' => 'Asset', 'icon' => 'user', 'items' => [
                                            ['label' => 'Category', 'icon' => 'user', 'url' => ['/aset-category/index']],
                                            ['label' => 'Subcategory', 'icon' => 'user', 'url' => ['/aset-subcategory/index']],
                                            ['label' => 'Inventory', 'icon' => 'user', 'url' => ['/aset-inventory/index']],
                                            ['label' => 'Inventory Item', 'icon' => 'user', 'url' => ['/aset-inventory-item/index']],
                                            ['label' => 'Instruction Order', 'icon' => 'user', 'url' => ['/aset-inst-order/index']],
                                            ['label' => 'Instruction Order Item', 'icon' => 'user', 'url' => ['/aset-inst-order-item/index']],
                                            ['label' => 'Instruction Maintenance', 'icon' => 'user', 'url' => ['/aset-inst-maintenance/index']],
                                            ['label' => 'Transaction', 'icon' => 'user', 'url' => ['/aset-transaction/index']],
                                            ['label' => 'Item Movement', 'icon' => 'user', 'url' => ['/aset-item-movement/index']],
                                        ]],
                                        ['label' => 'Inventory', 'icon' => 'user', 'items' => [
                                            ['label' => 'Store List', 'icon' => 'user', 'url' => ['/inv-store-list/index']],
                                            ['label' => 'Category', 'icon' => 'user', 'url' => ['/inv-category/index']],
                                            ['label' => 'Subcategory', 'icon' => 'user', 'url' => ['/inv-subcategory/index']],
                                            ['label' => 'Inventory', 'icon' => 'user', 'url' => ['/inv-inventory/index']],
                                            ['label' => 'Inventory Checkin', 'icon' => 'user', 'url' => ['/inv-inventory-checkin/index']],
                                            ['label' => 'Inventory Item', 'icon' => 'user', 'url' => ['/inv-inventory-item/index']],
                                            ['label' => 'Order', 'icon' => 'user', 'url' => ['/inv-order/index']],
                                            ['label' => 'Order Item', 'icon' => 'user', 'url' => ['/inv-order-item/index']],
                                            ['label' => 'Transaction', 'icon' => 'user', 'url' => ['/inv-transaction/index']],
                                            ['label' => 'Placement', 'icon' => 'user', 'url' => ['/inv-placement/index']],
                                            ['label' => 'Package', 'icon' => 'user', 'url' => ['/inv-package/index']],
                                        ]],
                                ],
                            ],
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                        ],
                    ],
                    // ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'visible'=>false,
                        'label' => 'Same tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
