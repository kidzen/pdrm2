<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetTransaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Transaction', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-transaction-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Aset Transaction'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'transaction_no',
        'type',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetInstMaintenance->totalCount){
    $gridColumnAsetInstMaintenance = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'inst_no',
            [
                'attribute' => 'vendor.name',
                'label' => 'Vendor'
            ],
            [
                'attribute' => 'item.id',
                'label' => 'Item'
            ],
            'type',
            'total_cost',
            'approval',
            'approved_by',
            'received_by',
            'requested_by',
            'requested_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetInstMaintenance,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-inst-maintenance']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Inst Maintenance'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetInstMaintenance
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetInstOrder->totalCount){
    $gridColumnAsetInstOrder = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'inst_no',
            [
                'attribute' => 'vendor.name',
                'label' => 'Vendor'
            ],
            'type',
            'approval',
            'approved_by',
            'received_by',
            'requested_by',
            'requested_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetInstOrder,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-inst-order']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Inst Order'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetInstOrder
    ]);
}
?>
    </div>
</div>
