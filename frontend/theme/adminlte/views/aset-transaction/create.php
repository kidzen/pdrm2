<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetTransaction */

$this->title = 'Create Aset Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Aset Transaction', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
