<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AsetTransaction */

$this->title = 'Update Aset Transaction: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Transaction', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aset-transaction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
