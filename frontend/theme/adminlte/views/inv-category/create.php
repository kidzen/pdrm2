<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvCategory */

$this->title = 'Create Inv Category';
$this->params['breadcrumbs'][] = ['label' => 'Inv Category', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
