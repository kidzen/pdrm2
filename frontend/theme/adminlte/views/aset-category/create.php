<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetCategory */

$this->title = 'Create Aset Category';
$this->params['breadcrumbs'][] = ['label' => 'Aset Category', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
