<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Aset Category', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-category-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Aset Category'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'type',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetSubcategory->totalCount){
    $gridColumnAsetSubcategory = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'name',
            'description',
            'type',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetSubcategory,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-subcategory']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Subcategory'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetSubcategory
    ]);
}
?>
    </div>
</div>
