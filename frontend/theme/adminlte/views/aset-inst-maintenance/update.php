<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AsetInstMaintenance */

$this->title = 'Update Aset Inst Maintenance: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Inst Maintenance', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aset-inst-maintenance-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
