<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetInstMaintenance */

$this->title = 'Create Aset Inst Maintenance';
$this->params['breadcrumbs'][] = ['label' => 'Aset Inst Maintenance', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-inst-maintenance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
