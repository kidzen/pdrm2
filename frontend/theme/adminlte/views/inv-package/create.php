<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvPackage */

$this->title = 'Create Inv Package';
$this->params['breadcrumbs'][] = ['label' => 'Inv Package', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
