<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InvOrderItem */

?>
<div class="inv-order-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'inventory.id',
            'label' => 'Inventory',
        ],
        [
            'attribute' => 'order.id',
            'label' => 'Order',
        ],
        'rq_quantity',
        'app_quantity',
        'current_balance',
        'unit_price',
        'string',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>