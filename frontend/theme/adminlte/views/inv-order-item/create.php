<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvOrderItem */

$this->title = 'Create Inv Order Item';
$this->params['breadcrumbs'][] = ['label' => 'Inv Order Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-order-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
