<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InvInventoryCheckin */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'InvInventoryItem', 
        'relID' => 'inv-inventory-item', 
        'value' => \yii\helpers\Json::encode($model->invInventoryItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="inv-inventory-checkin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'transaction_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\InvTransaction::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Inv transaction'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'inventory_id')->textInput(['placeholder' => 'Inventory']) ?>

    <?= $form->field($model, 'vendor_id')->textInput(['placeholder' => 'Vendor']) ?>

    <?= $form->field($model, 'items_quantity')->textInput(['placeholder' => 'Items Quantity']) ?>

    <?= $form->field($model, 'items_total_price')->textInput(['placeholder' => 'Items Total Price']) ?>

    <?= $form->field($model, 'check_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Check Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'check_by')->textInput(['placeholder' => 'Check By']) ?>

    <?= $form->field($model, 'approved')->textInput(['placeholder' => 'Approved']) ?>

    <?= $form->field($model, 'approved_by')->textInput(['placeholder' => 'Approved By']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('InvInventoryItem'),
            'content' => $this->render('_formInvInventoryItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->invInventoryItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
