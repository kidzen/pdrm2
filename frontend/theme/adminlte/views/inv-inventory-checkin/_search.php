<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\InvInventoryCheckinSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inv-inventory-checkin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'transaction_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\InvTransaction::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Inv transaction'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'inventory_id')->textInput(['placeholder' => 'Inventory']) ?>

    <?= $form->field($model, 'vendor_id')->textInput(['placeholder' => 'Vendor']) ?>

    <?= $form->field($model, 'items_quantity')->textInput(['placeholder' => 'Items Quantity']) ?>

    <?php /* echo $form->field($model, 'items_total_price')->textInput(['placeholder' => 'Items Total Price']) */ ?>

    <?php /* echo $form->field($model, 'check_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Check Date',
                'autoclose' => true
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'check_by')->textInput(['placeholder' => 'Check By']) */ ?>

    <?php /* echo $form->field($model, 'approved')->textInput(['placeholder' => 'Approved']) */ ?>

    <?php /* echo $form->field($model, 'approved_by')->textInput(['placeholder' => 'Approved By']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
