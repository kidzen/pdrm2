<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InvInventoryCheckin */

$this->title = 'Update Inv Inventory Checkin: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Inventory Checkin', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inv-inventory-checkin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
