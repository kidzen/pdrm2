<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InvInventoryCheckin */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Inventory Checkin', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-inventory-checkin-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inv Inventory Checkin'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'transaction.id',
            'label' => 'Transaction',
        ],
        'inventory_id',
        'vendor_id',
        'items_quantity',
        'items_total_price',
        'check_date',
        'check_by',
        'approved',
        'approved_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInvInventoryItem->totalCount){
    $gridColumnInvInventoryItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'inventory.id',
                'label' => 'Inventory'
            ],
                        [
                'attribute' => 'checkout.id',
                'label' => 'Checkout'
            ],
            'sku',
            'unit_price',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInvInventoryItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-inventory-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Inventory Item'),
        ],
        'export' => false,
        'columns' => $gridColumnInvInventoryItem
    ]);
}
?>
    </div>
</div>
