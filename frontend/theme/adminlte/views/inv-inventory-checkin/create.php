<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvInventoryCheckin */

$this->title = 'Create Inv Inventory Checkin';
$this->params['breadcrumbs'][] = ['label' => 'Inv Inventory Checkin', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-inventory-checkin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
