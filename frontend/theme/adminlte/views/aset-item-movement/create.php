<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetItemMovement */

$this->title = 'Create Aset Item Movement';
$this->params['breadcrumbs'][] = ['label' => 'Aset Item Movement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-item-movement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
