<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AsetItemMovement */

$this->title = 'Update Aset Item Movement: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Item Movement', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aset-item-movement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
