<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetInventoryItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Inventory Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-inventory-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Aset Inventory Item'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'inventory.name',
            'label' => 'Inventory',
        ],
        'parent_id',
        'serial_no',
        'model',
        'cost',
        [
            'attribute' => 'warranty.id',
            'label' => 'Warranty',
        ],
        'asset_class',
        'type',
        'engine_no',
        'casis_no',
        'manufacture_no',
        'reg_no',
        'date_received',
        'order_no_reff',
        'order_date_reff',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetInstMaintenance->totalCount){
    $gridColumnAsetInstMaintenance = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 't.id',
                'label' => 'T'
            ],
            'inst_no',
            [
                'attribute' => 'vendor.name',
                'label' => 'Vendor'
            ],
                        'type',
            'total_cost',
            'approval',
            'approved_by',
            'received_by',
            'requested_by',
            'requested_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetInstMaintenance,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-inst-maintenance']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Inst Maintenance'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetInstMaintenance
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetItemMovement->totalCount){
    $gridColumnAsetItemMovement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'reff_id',
            'type',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetItemMovement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-item-movement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Item Movement'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetItemMovement
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetItemPlacement->totalCount){
    $gridColumnAsetItemPlacement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'itemMovement.id',
                'label' => 'Item Movement'
            ],
            [
                'attribute' => 'site.name',
                'label' => 'Site'
            ],
            'type',
            'approval',
            'approved_by',
            'received_by',
            'requested_by',
            'requested_at',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetItemPlacement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-item-placement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Item Placement'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetItemPlacement
    ]);
}
?>
    </div>
</div>
