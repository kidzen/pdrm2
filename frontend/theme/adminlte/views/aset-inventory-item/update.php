<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AsetInventoryItem */

$this->title = 'Update Aset Inventory Item: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Inventory Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aset-inventory-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
