<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('AsetInventoryItem'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Aset Inst Maintenance'),
        'content' => $this->render('_dataAsetInstMaintenance', [
            'model' => $model,
            'row' => $model->asetInstMaintenances,
        ]),
    ],
                    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Aset Item Movement'),
        'content' => $this->render('_dataAsetItemMovement', [
            'model' => $model,
            'row' => $model->asetItemMovements,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Aset Item Placement'),
        'content' => $this->render('_dataAsetItemPlacement', [
            'model' => $model,
            'row' => $model->asetItemPlacements,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
