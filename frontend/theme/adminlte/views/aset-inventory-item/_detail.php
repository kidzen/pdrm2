<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetInventoryItem */

?>
<div class="aset-inventory-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'inventory.name',
            'label' => 'Inventory',
        ],
        'parent_id',
        'serial_no',
        'model',
        'cost',
        [
            'attribute' => 'warranty.id',
            'label' => 'Warranty',
        ],
        'asset_class',
        'type',
        'engine_no',
        'casis_no',
        'manufacture_no',
        'reg_no',
        'date_received',
        'order_no_reff',
        'order_date_reff',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>