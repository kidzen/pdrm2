<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetInventoryItem */

$this->title = 'Create Aset Inventory Item';
$this->params['breadcrumbs'][] = ['label' => 'Aset Inventory Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-inventory-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
