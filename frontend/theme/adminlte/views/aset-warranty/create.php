<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetWarranty */

$this->title = 'Create Aset Warranty';
$this->params['breadcrumbs'][] = ['label' => 'Aset Warranty', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-warranty-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
