<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetWarranty */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Warranty', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-warranty-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Aset Warranty'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'vendor.name',
            'label' => 'Vendor',
        ],
        'start_date',
        'end_date',
        'type',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetInventoryItem->totalCount){
    $gridColumnAsetInventoryItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'inventory.name',
                'label' => 'Inventory'
            ],
            'parent_id',
            'serial_no',
            'model',
            'cost',
                        'asset_class',
            'type',
            'engine_no',
            'casis_no',
            'manufacture_no',
            'reg_no',
            'date_received',
            'order_no_reff',
            'order_date_reff',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetInventoryItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-inventory-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Inventory Item'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetInventoryItem
    ]);
}
?>
    </div>
</div>
