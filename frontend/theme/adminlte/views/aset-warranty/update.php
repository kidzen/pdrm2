<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AsetWarranty */

$this->title = 'Update Aset Warranty: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Warranty', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aset-warranty-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
