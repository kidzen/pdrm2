<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->asetInventoryItems,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'inventory.name',
                'label' => 'Inventory'
            ],
        'parent_id',
        'serial_no',
        'model',
        'cost',
        'asset_class',
        'type',
        'engine_no',
        'casis_no',
        'manufacture_no',
        'reg_no',
        'date_received',
        'order_no_reff',
        'order_date_reff',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'aset-inventory-item'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
