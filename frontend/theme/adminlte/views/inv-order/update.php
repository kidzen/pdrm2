<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InvOrder */

$this->title = 'Update Inv Order: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inv-order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
