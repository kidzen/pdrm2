<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InvOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-order-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inv Order'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'transaction_id',
        'arahan_kerja_id',
        'order_date',
        'ordered_by',
        'order_no',
        'required_date',
        'checkout_date',
        'checkout_by',
        'approved',
        'approved_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInvOrderItem->totalCount){
    $gridColumnInvOrderItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'inventory.id',
                'label' => 'Inventory'
            ],
                        'rq_quantity',
            'app_quantity',
            'current_balance',
            'unit_price',
            'string',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInvOrderItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-order-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Order Item'),
        ],
        'export' => false,
        'columns' => $gridColumnInvOrderItem
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerInvPackage->totalCount){
    $gridColumnInvPackage = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'detail',
            'delivery',
            'package_by',
            'package_date',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInvPackage,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-package']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Package'),
        ],
        'export' => false,
        'columns' => $gridColumnInvPackage
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerInvPlacement->totalCount){
    $gridColumnInvPlacement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'type',
                        [
                'attribute' => 'store.name',
                'label' => 'Store'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInvPlacement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-placement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Placement'),
        ],
        'export' => false,
        'columns' => $gridColumnInvPlacement
    ]);
}
?>
    </div>
</div>
