<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InvOrder */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'InvOrderItem', 
        'relID' => 'inv-order-item', 
        'value' => \yii\helpers\Json::encode($model->invOrderItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'InvPackage', 
        'relID' => 'inv-package', 
        'value' => \yii\helpers\Json::encode($model->invPackages),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'InvPlacement', 
        'relID' => 'inv-placement', 
        'value' => \yii\helpers\Json::encode($model->invPlacements),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="inv-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'transaction_id')->textInput(['placeholder' => 'Transaction']) ?>

    <?= $form->field($model, 'arahan_kerja_id')->textInput(['placeholder' => 'Arahan Kerja']) ?>

    <?= $form->field($model, 'order_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Order Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'ordered_by')->textInput(['maxlength' => true, 'placeholder' => 'Ordered By']) ?>

    <?= $form->field($model, 'order_no')->textInput(['maxlength' => true, 'placeholder' => 'Order No']) ?>

    <?= $form->field($model, 'required_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Required Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'checkout_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Checkout Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'checkout_by')->textInput(['placeholder' => 'Checkout By']) ?>

    <?= $form->field($model, 'approved')->textInput(['placeholder' => 'Approved']) ?>

    <?= $form->field($model, 'approved_by')->textInput(['placeholder' => 'Approved By']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('InvOrderItem'),
            'content' => $this->render('_formInvOrderItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->invOrderItems),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('InvPackage'),
            'content' => $this->render('_formInvPackage', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->invPackages),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('InvPlacement'),
            'content' => $this->render('_formInvPlacement', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->invPlacements),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
