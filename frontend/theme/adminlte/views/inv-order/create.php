<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvOrder */

$this->title = 'Create Inv Order';
$this->params['breadcrumbs'][] = ['label' => 'Inv Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
