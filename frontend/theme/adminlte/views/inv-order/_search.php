<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\InvOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inv-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'transaction_id')->textInput(['placeholder' => 'Transaction']) ?>

    <?= $form->field($model, 'arahan_kerja_id')->textInput(['placeholder' => 'Arahan Kerja']) ?>

    <?= $form->field($model, 'order_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Order Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'ordered_by')->textInput(['maxlength' => true, 'placeholder' => 'Ordered By']) ?>

    <?php /* echo $form->field($model, 'order_no')->textInput(['maxlength' => true, 'placeholder' => 'Order No']) */ ?>

    <?php /* echo $form->field($model, 'required_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Required Date',
                'autoclose' => true
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'checkout_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Checkout Date',
                'autoclose' => true
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'checkout_by')->textInput(['placeholder' => 'Checkout By']) */ ?>

    <?php /* echo $form->field($model, 'approved')->textInput(['placeholder' => 'Approved']) */ ?>

    <?php /* echo $form->field($model, 'approved_by')->textInput(['placeholder' => 'Approved By']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
