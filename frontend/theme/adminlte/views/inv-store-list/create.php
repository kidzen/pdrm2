<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvStoreList */

$this->title = 'Create Inv Store List';
$this->params['breadcrumbs'][] = ['label' => 'Inv Store List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-store-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
