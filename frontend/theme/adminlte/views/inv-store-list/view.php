<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InvStoreList */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Inv Store List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-store-list-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inv Store List'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'location',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInvPlacement->totalCount){
    $gridColumnInvPlacement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'type',
            [
                'attribute' => 'order.id',
                'label' => 'Order'
            ],
                ];
    echo Gridview::widget([
        'dataProvider' => $providerInvPlacement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-placement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Placement'),
        ],
        'export' => false,
        'columns' => $gridColumnInvPlacement
    ]);
}
?>
    </div>
</div>
