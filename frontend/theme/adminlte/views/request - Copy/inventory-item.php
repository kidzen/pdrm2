<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\InventoryItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Inventory Item';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="inventory-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Inventory Item', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_ii_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_ii_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'inventory_id',
                'label' => 'Inventory',
                'value' => function($model){
                    if ($model->inventory)
                    {return $model->inventory->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Inventory::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Inventory', 'id' => 'grid-inventory-item-search-inventory_id']
            ],
        'serial_no',
        'manufacture_no',
        'cost',
        'warranty_id',
        'type',
        'remark',
        'status',
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{maintenance} {dispose}',
            // 'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
            // 'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
            'buttons' => [
                'maintenance' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-wrench"></span>', ['maintenance', 'id' => $model->id], ['title' => Yii::t('yii', 'Maintenance'), 'data-toggle' => 'tooltip','data-method' => 'POST']);
                },
                        'dispose' => function ($url, $model) {
//                        if ($model->DELETED === 0) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['dispose', 'id' => $model->id], ['title' => Yii::t('yii', 'Dispose'), 'data-toggle' => 'tooltip',
                                'data-method' => 'post']);
                       // }
                }
                ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inventory-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>

