<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->instMaintenances,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 't.id',
                'label' => 'T'
            ],
        'inst_no',
        [
                'attribute' => 'vendor.name',
                'label' => 'Vendor'
            ],
        'type',
        'total_cost',
        'approval',
        'approved_by',
        'approved_at',
        'received_by',
        'received_at',
        'requested_by',
        'requested_at',
        'remark',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'inst-maintenance'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
