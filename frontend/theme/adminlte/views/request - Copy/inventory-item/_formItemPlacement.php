<div class="form-group" id="add-item-placement">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'ItemPlacement',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'item_movement_id' => [
            'label' => 'Item movement',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\ItemMovement::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Item movement'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'site_id' => [
            'label' => 'Sites',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\Sites::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Sites'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'type' => ['type' => TabularForm::INPUT_TEXT],
        'approval' => ['type' => TabularForm::INPUT_TEXT],
        'approved_by' => ['type' => TabularForm::INPUT_TEXT],
        'approved_at' => ['type' => TabularForm::INPUT_TEXT],
        'received_by' => ['type' => TabularForm::INPUT_TEXT],
        'received_at' => ['type' => TabularForm::INPUT_TEXT],
        'requested_by' => ['type' => TabularForm::INPUT_TEXT],
        'requested_at' => ['type' => TabularForm::INPUT_TEXT],
        'remark' => ['type' => TabularForm::INPUT_TEXT],
        'status' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowItemPlacement(' . $key . '); return false;', 'id' => 'item-placement-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Item Placement', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowItemPlacement()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

