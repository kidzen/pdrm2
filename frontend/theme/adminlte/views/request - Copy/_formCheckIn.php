<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InstOrder */
/* @var $form yii\widgets\ActiveForm */
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'AsetInstOrderItem',
        'relID' => 'aset-inst-order-item',
        'value' => \yii\helpers\Json::encode($model->instOrderItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="inst-order-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>

    <div class="row">

    <div class="col-md-3">
    <?= $form->field($model, 'inst_no')->textInput(['maxlength' => true, 'placeholder' => 'Inst No','readOnly'=>true]) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'vendor_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Vendor::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Vendor'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'type')->textInput(['placeholder' => 'Type']) ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-3">
    <?= $form->field($model, 'remark')->textArea(['maxlength' => true, 'placeholder' => 'Remark']) ?>
    </div>
    </div>
    <!-- <div class="clear-fix"></div> -->
    <!-- <div> -->
    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('InstOrderItem'),
            'content' => $this->render('_formCheckInItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->instOrderItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
