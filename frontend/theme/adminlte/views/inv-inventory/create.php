<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvInventory */

$this->title = 'Create Inv Inventory';
$this->params['breadcrumbs'][] = ['label' => 'Inv Inventory', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-inventory-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
