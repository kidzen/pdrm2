<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InvInventory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Inventory', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-inventory-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inv Inventory'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'subcategory.name',
            'label' => 'Subcategory',
        ],
        'card_no',
        'code_no',
        'description',
        'quantity',
        'min_stock',
        'location',
        'approved',
        'approved_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInvInventoryItem->totalCount){
    $gridColumnInvInventoryItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'checkin.id',
                'label' => 'Checkin'
            ],
            [
                'attribute' => 'checkout.id',
                'label' => 'Checkout'
            ],
            'sku',
            'unit_price',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInvInventoryItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-inventory-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Inventory Item'),
        ],
        'export' => false,
        'columns' => $gridColumnInvInventoryItem
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerInvOrderItem->totalCount){
    $gridColumnInvOrderItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'order.id',
                'label' => 'Order'
            ],
            'rq_quantity',
            'app_quantity',
            'current_balance',
            'unit_price',
            'string',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInvOrderItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-order-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Order Item'),
        ],
        'export' => false,
        'columns' => $gridColumnInvOrderItem
    ]);
}
?>
    </div>
</div>
