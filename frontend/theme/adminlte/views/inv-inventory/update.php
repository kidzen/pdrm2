<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InvInventory */

$this->title = 'Update Inv Inventory: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Inventory', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inv-inventory-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
