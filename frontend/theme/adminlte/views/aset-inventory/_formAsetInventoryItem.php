<div class="form-group" id="add-aset-inventory-item">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'AsetInventoryItem',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'parent_id' => ['type' => TabularForm::INPUT_TEXT],
        'serial_no' => ['type' => TabularForm::INPUT_TEXT],
        'model' => ['type' => TabularForm::INPUT_TEXT],
        'cost' => ['type' => TabularForm::INPUT_TEXT],
        'warranty_id' => [
            'label' => 'Aset warranty',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\AsetWarranty::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Aset warranty'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'asset_class' => ['type' => TabularForm::INPUT_TEXT],
        'type' => ['type' => TabularForm::INPUT_TEXT],
        'engine_no' => ['type' => TabularForm::INPUT_TEXT],
        'casis_no' => ['type' => TabularForm::INPUT_TEXT],
        'manufacture_no' => ['type' => TabularForm::INPUT_TEXT],
        'reg_no' => ['type' => TabularForm::INPUT_TEXT],
        'date_received' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Date Received',
                        'autoclose' => true
                    ]
                ],
            ]
        ],
        'order_no_reff' => ['type' => TabularForm::INPUT_TEXT],
        'order_date_reff' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Order Date Reff',
                        'autoclose' => true
                    ]
                ],
            ]
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowAsetInventoryItem(' . $key . '); return false;', 'id' => 'aset-inventory-item-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Aset Inventory Item', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAsetInventoryItem()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

