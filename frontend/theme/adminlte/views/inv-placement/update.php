<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InvPlacement */

$this->title = 'Update Inv Placement: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Placement', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inv-placement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
