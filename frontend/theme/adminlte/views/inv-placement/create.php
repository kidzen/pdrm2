<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvPlacement */

$this->title = 'Create Inv Placement';
$this->params['breadcrumbs'][] = ['label' => 'Inv Placement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-placement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
