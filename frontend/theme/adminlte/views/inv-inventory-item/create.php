<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvInventoryItem */

$this->title = 'Create Inv Inventory Item';
$this->params['breadcrumbs'][] = ['label' => 'Inv Inventory Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-inventory-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
