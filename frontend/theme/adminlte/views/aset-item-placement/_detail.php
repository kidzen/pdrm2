<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetItemPlacement */

?>
<div class="aset-item-placement-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'item.id',
            'label' => 'Item',
        ],
        [
            'attribute' => 'itemMovement.id',
            'label' => 'Item Movement',
        ],
        [
            'attribute' => 'site.name',
            'label' => 'Site',
        ],
        'type',
        'approval',
        'approved_by',
        'received_by',
        'requested_by',
        'requested_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>