<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetItemPlacement */

$this->title = 'Create Aset Item Placement';
$this->params['breadcrumbs'][] = ['label' => 'Aset Item Placement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-item-placement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
