<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AsetInstOrder */

$this->title = 'Create Aset Inst Order';
$this->params['breadcrumbs'][] = ['label' => 'Aset Inst Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-inst-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
