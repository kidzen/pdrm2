<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AsetInstOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-aset-inst-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 't_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\AsetTransaction::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Aset transaction'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'inst_no')->textInput(['maxlength' => true, 'placeholder' => 'Inst No']) ?>

    <?= $form->field($model, 'vendor_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Vendor::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Vendor'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'type')->textInput(['placeholder' => 'Type']) ?>

    <?php /* echo $form->field($model, 'approval')->textInput(['placeholder' => 'Approval']) */ ?>

    <?php /* echo $form->field($model, 'approved_by')->textInput(['placeholder' => 'Approved By']) */ ?>

    <?php /* echo $form->field($model, 'received_by')->textInput(['placeholder' => 'Received By']) */ ?>

    <?php /* echo $form->field($model, 'requested_by')->textInput(['placeholder' => 'Requested By']) */ ?>

    <?php /* echo $form->field($model, 'requested_at')->textInput(['placeholder' => 'Requested At']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
