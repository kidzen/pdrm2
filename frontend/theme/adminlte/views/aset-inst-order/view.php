<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AsetInstOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aset Inst Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aset-inst-order-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Aset Inst Order'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 't.id',
            'label' => 'T',
        ],
        'inst_no',
        [
            'attribute' => 'vendor.name',
            'label' => 'Vendor',
        ],
        'type',
        'approval',
        'approved_by',
        'received_by',
        'requested_by',
        'requested_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAsetInstOrderItem->totalCount){
    $gridColumnAsetInstOrderItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'inventory.name',
                'label' => 'Inventory'
            ],
            'delivery_no',
            'received_date',
            'quantity_request',
            'quantity_received',
            'price_per_item',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsetInstOrderItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aset-inst-order-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Aset Inst Order Item'),
        ],
        'export' => false,
        'columns' => $gridColumnAsetInstOrderItem
    ]);
}
?>
    </div>
</div>
