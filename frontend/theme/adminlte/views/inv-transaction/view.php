<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InvTransaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Transaction', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-transaction-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inv Transaction'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'type',
        'check_date',
        'check_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerInvInventoryCheckin->totalCount){
    $gridColumnInvInventoryCheckin = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'inventory_id',
            'vendor_id',
            'items_quantity',
            'items_total_price',
            'check_date',
            'check_by',
            'approved',
            'approved_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInvInventoryCheckin,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv-inventory-checkin']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv Inventory Checkin'),
        ],
        'export' => false,
        'columns' => $gridColumnInvInventoryCheckin
    ]);
}
?>
    </div>
</div>
