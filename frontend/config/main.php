<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'view' => [
            'theme' => [
                // 'basePath' => '@frontend/theme/gentella',
                // 'baseUrl' => '@web/themes/basic',
                'pathMap' => [
                    '@frontend/views' => '@frontend/theme/adminlte/views',
                    // '@frontend/views' => '@frontend/theme/gentella/views',
                    // '@frontend/views' => '@frontend/theme/'.$params['themes'].'/views',
                ],
            ],
        ],
        'notify' => [
            'class' => 'common\components\Notify',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'class' => 'common\components\User',
            'identityClass' => 'common\models\identity\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'pdrm2-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        // 'log' => [
        //     'targets' => [
        //         [
        //             'class' => 'yii\log\DbTarget',
        //             'levels' => ['error', 'warning'],
        //         ],
        //         [
        //             'class' => 'yii\log\EmailTarget',
        //             'levels' => ['error'],
        //             'categories' => ['yii\db\*'],
        //             'message' => [
        //                'from' => ['log@example.com'],
        //                'to' => ['admin@example.com', 'developer@example.com'],
        //                'subject' => 'Database errors at example.com',
        //             ],
        //         ],
        //     ],
        // ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        // If you use tree table
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        // see settings on http://demos.krajee.com/tree-manager#module
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
        ],
        'dynagrid' => [
            'class' => '\kartik\dynagrid\Module',
        ],
        'debug' => ['class' => 'yii\debug\Module'], //remove on production
        'gii' => [
            'class' => 'yii\gii\Module',
            'generators' => [//here
                'crud' => [// generator name
                    'class' => 'yii\gii\generators\crud\Generator', // generator class
                    'templates' => [//setting for out templates
                        'myCrud' => '@frontend/components/templates/crud/default', // template name => path to template
                    ]
                ]
            ],
        ],
    ],
    'params' => $params,
];
