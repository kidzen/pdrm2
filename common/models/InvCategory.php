<?php

namespace common\models;

use \common\models\base\InvCategory as BaseInvCategory;

/**
 * This is the model class for table "inv_category".
 */
class InvCategory extends BaseInvCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['deleted', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ]);
    }
	
}
