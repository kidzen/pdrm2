<?php

namespace common\models;

use \common\models\base\InvInventory as BaseInvInventory;

/**
 * This is the model class for table "inv_inventory".
 */
class InvInventory extends BaseInvInventory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['subcategory_id', 'quantity', 'min_stock', 'approved', 'approved_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['card_no', 'code_no', 'description', 'location'], 'string', 'max' => 255]
        ]);
    }
	
}
