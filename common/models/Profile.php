<?php

namespace common\models;

use \common\models\base\Profile as BaseProfile;

/**
 * This is the model class for table "profile".
 */
class Profile extends BaseProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'name'], 'required'],
            [['user_id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'position', 'department', 'remark'], 'string', 'max' => 255],
            [['user_id'], 'unique']
        ]);
    }
	
}
