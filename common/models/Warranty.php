<?php

namespace common\models;

use \common\models\base\Warranty as BaseWarranty;

/**
 * This is the model class for table "warranty".
 */
class Warranty extends BaseWarranty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['vendor_id', 'type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['remark'], 'string', 'max' => 255]
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'vendor_id' => 'Vendor ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'type' => 'Type',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
}
