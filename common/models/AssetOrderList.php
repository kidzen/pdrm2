<?php

namespace common\models;

use \common\models\base\AssetOrderList as BaseAssetOrderList;

/**
 * This is the model class for table "asset_order_list".
 */
class AssetOrderList extends BaseAssetOrderList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'inventory_id', 'inst_order_id', 'category_id', 'quantity_request', 'quantity_received', 'io_status', 'ioi_status'], 'integer'],
            [['price_per_item'], 'number'],
            [['category', 'subcategory', 'inventory', 'inst_no'], 'string', 'max' => 255]
        ]);
    }

    public static function received($id) {
        $data = AsetInstOrderItem::findOne($id);
        $data->quantity_received = $data->quantity_request;
        $data->status = $data::STATUS_APPROVED;
        // $data->item_movement_id =
        AsetInventoryItem::add($data->quantity_received,$data);
        return $data->save();
    }

    public static function reject($id) {
        $data = AsetInstOrderItem::findOne($id);
        $data->status = $data::STATUS_REJECTED;
        return $data->save();
    }

}
