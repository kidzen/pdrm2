<?php

namespace common\models;

use \common\models\base\AsetSubcategory as BaseAsetSubcategory;

/**
 * This is the model class for table "aset_subcategory".
 */
class AsetSubcategory extends BaseAsetSubcategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['category_id', 'type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'description', 'remark'], 'string', 'max' => 255],
            [['name'], 'unique']
        ]);
    }
	
}
