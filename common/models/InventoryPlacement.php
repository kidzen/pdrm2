<?php

namespace common\models;

use \common\models\base\InventoryPlacement as BaseInventoryPlacement;

/**
 * This is the model class for table "inventory_placement".
 */
class InventoryPlacement extends BaseInventoryPlacement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['asset_id', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['location', 'remark'], 'string', 'max' => 255],
            [['location'], 'unique']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'asset_id' => 'Asset ID',
            'location' => 'Location',
            'approval' => 'Approval',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'received_by' => 'Received By',
            'received_at' => 'Received At',
            'requested_by' => 'Requested By',
            'requested_at' => 'Requested At',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
}
