<?php

namespace common\models;

use \common\models\base\InvPackage as BaseInvPackage;

/**
 * This is the model class for table "inv_package".
 */
class InvPackage extends BaseInvPackage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['order_id', 'package_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['package_date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['detail', 'delivery'], 'string', 'max' => 255]
        ]);
    }
	
}
