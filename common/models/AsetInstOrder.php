<?php

namespace common\models;

use \common\models\custom\AsetInstOrder as BaseAsetInstOrder;

/**
 * This is the model class for table "aset_inst_order".
 */
class AsetInstOrder extends BaseAsetInstOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['t_id', 'vendor_id', 'type', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['inst_no', 'remark'], 'string', 'max' => 255],
            [['inst_no'], 'unique']
        ]);
    }

}
