<?php

namespace common\models;

use \common\models\base\InvOrderItem as BaseInvOrderItem;

/**
 * This is the model class for table "inv_order_item".
 */
class InvOrderItem extends BaseInvOrderItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['inventory_id', 'order_id', 'rq_quantity', 'app_quantity', 'current_balance', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['string'], 'string', 'max' => 255]
        ]);
    }
	
}
