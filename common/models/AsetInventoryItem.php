<?php

namespace common\models;

use \common\models\base\AsetInventoryItem as BaseAsetInventoryItem;

/**
 * This is the model class for table "aset_inventory_item".
 */
class AsetInventoryItem extends BaseAsetInventoryItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
            [
            [['inventory_id', 'parent_id', 'warranty_id', 'asset_class', 'type', 'engine_no', 'casis_no', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['date_received', 'order_date_reff', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['serial_no', 'model', 'cost', 'manufacture_no', 'reg_no', 'order_no_reff', 'remark'], 'string', 'max' => 255],
            [['serial_no'], 'unique']
            ]);
    }
    public function generateSerialNo() {
        $data = static::find()->max("CAST(SUBSTR(serial_no,9) AS UNSIGNED)");
        $bil = $data + 1;
        $bilPrefix = date('Y-m-');
        return $bilPrefix.$bil;
    }
    public static function add($quantity,$instOrderItem) {

        for($i=$quantity;$i>0;$i--){
            $data = new static();
            $data->serial_no = $data->generateSerialNo();
            $data->inventory_id = $instOrderItem->inventory->id;
            $data->save();
            ItemMovement::add($data,$instOrderItem,1);
        }
        return true;

    }
    public static function dispose($id) {

        $model = static::findOne($id);
        $model->status = 9;
        $model->save();
        return true;

    }

}
