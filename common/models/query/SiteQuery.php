<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\Site]].
 *
 * @see \common\models\query\Site
 */
class SiteQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\query\Site[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\Site|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}