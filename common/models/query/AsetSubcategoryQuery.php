<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\AsetSubcategory]].
 *
 * @see \common\models\query\AsetSubcategory
 */
class AsetSubcategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\query\AsetSubcategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AsetSubcategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}