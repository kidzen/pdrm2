<?php

namespace common\models;

use \common\models\base\InvOrder as BaseInvOrder;

/**
 * This is the model class for table "inv_order".
 */
class InvOrder extends BaseInvOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['transaction_id', 'arahan_kerja_id', 'checkout_by', 'approved', 'approved_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['order_date', 'required_date', 'checkout_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['ordered_by', 'order_no'], 'string', 'max' => 255]
        ]);
    }
	
}
