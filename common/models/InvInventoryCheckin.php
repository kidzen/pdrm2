<?php

namespace common\models;

use \common\models\base\InvInventoryCheckin as BaseInvInventoryCheckin;

/**
 * This is the model class for table "inv_inventory_checkin".
 */
class InvInventoryCheckin extends BaseInvInventoryCheckin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['transaction_id', 'inventory_id', 'vendor_id', 'items_quantity', 'check_by', 'approved', 'approved_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['items_total_price'], 'number'],
            [['check_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
}
