<?php

namespace common\models;

use \common\models\custom\AsetTransaction as BaseAsetTransaction;

/**
 * This is the model class for table "aset_transaction".
 */
class AsetTransaction extends BaseAsetTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['transaction_no', 'remark'], 'string', 'max' => 255],
            [['transaction_no'], 'unique']
        ]);
    }

}
