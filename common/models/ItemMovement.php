<?php

namespace common\models;

use \common\models\base\ItemMovement as BaseItemMovement;

/**
 * This is the model class for table "item_movement".
 */
class ItemMovement extends BaseItemMovement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
           [
           [['item_id', 'reff_id', 'type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
           [['created_at', 'updated_at', 'deleted_at'], 'safe'],
           [['remark'], 'string', 'max' => 255]
           ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        'id' => 'ID',
        'item_id' => 'Item ID',
        'reff_id' => 'Reff ID',
        'type' => 'Type',
        'remark' => 'Remark',
        'status' => 'Status',
        ];
    }
    public static function add($item,$refModel,$type) {
     $data = new static();
       // var_dump($item->id);die();
     $data->item_id = $item->id;
     $data->type = $type;
       // $data->delivery_no = $data->generateDeliveryNo();
     if($type == 1){
         $instOrderitem = $refModel;
           // var_dump($instOrderitem->instOrder->id);die();
         $data->reff_id = $instOrderitem->instOrder->id;
           // $data->getErrors();
     }
     $data->save();
     return true;
       // if($data->save()){
       //    return $data->id;
       // } else {
       //    var_dump($data->getErrors());
       //    echo 'fail to save';die();
       // }
       // return null;
 }
}
