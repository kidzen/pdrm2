<?php

namespace common\models;

use \common\models\base\AsetItemMovement as BaseAsetItemMovement;

/**
 * This is the model class for table "aset_item_movement".
 */
class AsetItemMovement extends BaseAsetItemMovement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
           [
           [['item_id', 'reff_id', 'type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
           [['created_at', 'updated_at', 'deleted_at'], 'safe'],
           [['remark'], 'string', 'max' => 255]
           ]);
    }

    public static function add($item,$refModel,$type) {
        $data = new static();
        // var_dump($item->id);die();
        $data->item_id = $item->id;
        $data->type = $type;
        // $data->delivery_no = $data->generateDeliveryNo();
        if($type == 1){
            $instOrderitem = $refModel;
            // var_dump($instOrderitem->instOrder->id);die();
            $data->reff_id = $instOrderitem->instOrder->id;
            // $data->getErrors();
        }
        $data->save();
        return true;
        // if($data->save()){
        //    return $data->id;
        // } else {
        //    var_dump($data->getErrors());
        //    echo 'fail to save';die();
        // }
        // return null;
    }
}
