<?php

namespace common\models;

use \common\models\base\InvSubcategory as BaseInvSubcategory;

/**
 * This is the model class for table "inv_subcategory".
 */
class InvSubcategory extends BaseInvSubcategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['category_id', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ]);
    }
	
}
