<?php

namespace common\models;

use \common\models\base\AsetInstOrderItem as BaseAsetInstOrderItem;

/**
 * This is the model class for table "aset_inst_order_item".
 */
class AsetInstOrderItem extends BaseAsetInstOrderItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['inst_order_id', 'inventory_id', 'quantity_request', 'quantity_received', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['price_per_item'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['delivery_no', 'received_date', 'remark'], 'string', 'max' => 255]
        ]);
    }
	
}
