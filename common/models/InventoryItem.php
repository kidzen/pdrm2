<?php

namespace common\models;

use \common\models\base\InventoryItem as BaseInventoryItem;

/**
 * This is the model class for table "inventory_item".
 */
class InventoryItem extends BaseInventoryItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['inventory_id', 'parent_id', 'warranty_id', 'type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['serial_no', 'manufacture_no', 'cost', 'remark'], 'string', 'max' => 255],
            [['serial_no'], 'unique']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'inventory_id' => 'Inventory ID',
            'parent_id' => 'Parent ID',
            'serial_no' => 'Serial No',
            'manufacture_no' => 'Manufacture No',
            'cost' => 'Cost',
            'warranty_id' => 'Warranty ID',
            'type' => 'Type',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }

    public function generateSerialNo() {
       $data = static::find()->max("CAST(SUBSTR(serial_no,9) AS UNSIGNED)");
       $bil = $data + 1;
       $bilPrefix = date('Y-m-');
       return $bilPrefix.$bil;
   }
   public static function add($quantity,$instOrderItem) {

       for($i=$quantity;$i>0;$i--){
           $data = new static();
           $data->serial_no = $data->generateSerialNo();
           $data->inventory_id = $instOrderItem->inventory->id;
           $data->save();
           ItemMovement::add($data,$instOrderItem,1);
       }
       return true;

   }
   public static function dispose($id) {

      $model = static::findOne($id);
      $model->status = 9;
      $model->save();
       return true;

   }
}
