<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "item_placement".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $item_movement_id
 * @property integer $site_id
 * @property integer $type
 * @property integer $approval
 * @property integer $approved_by
 * @property string $approved_at
 * @property integer $received_by
 * @property string $received_at
 * @property integer $requested_by
 * @property string $requested_at
 * @property string $remark
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \common\models\InventoryItem $item
 * @property \common\models\Sites $site
 * @property \common\models\ItemMovement $itemMovement
 */
class ItemPlacement extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'item_movement_id', 'site_id', 'type', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['remark'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_placement';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'item_movement_id' => 'Item Movement ID',
            'site_id' => 'Site ID',
            'type' => 'Type',
            'approval' => 'Approval',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'received_by' => 'Received By',
            'received_at' => 'Received At',
            'requested_by' => 'Requested By',
            'requested_at' => 'Requested At',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(\common\models\InventoryItem::className(), ['id' => 'item_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(\common\models\Sites::className(), ['id' => 'site_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemMovement()
    {
        return $this->hasOne(\common\models\ItemMovement::className(), ['id' => 'item_movement_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ItemPlacementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ItemPlacementQuery(get_called_class());
    }
}
