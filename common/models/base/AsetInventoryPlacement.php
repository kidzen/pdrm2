<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "aset_inventory_placement".
 *
 * @property integer $id
 * @property integer $asset_id
 * @property string $location
 * @property integer $approval
 * @property integer $approved_by
 * @property string $approved_at
 * @property integer $received_by
 * @property string $received_at
 * @property integer $requested_by
 * @property string $requested_at
 * @property string $remark
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 */
class AsetInventoryPlacement extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asset_id', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['location', 'remark'], 'string', 'max' => 255],
            [['location'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aset_inventory_placement';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'asset_id' => 'Asset ID',
            'location' => 'Location',
            'approval' => 'Approval',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'received_by' => 'Received By',
            'received_at' => 'Received At',
            'requested_by' => 'Requested By',
            'requested_at' => 'Requested At',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }

/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AsetInventoryPlacementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AsetInventoryPlacementQuery(get_called_class());
    }
}
