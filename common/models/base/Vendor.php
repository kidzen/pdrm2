<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vendor".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property integer $phone
 * @property integer $fax
 * @property string $email
 * @property string $remark
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \common\models\AsetInstMaintenance[] $asetInstMaintenances
 * @property \common\models\AsetInstOrder[] $asetInstOrders
 * @property \common\models\AsetWarranty[] $asetWarranties
 */
class Vendor extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'fax', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'address', 'email', 'remark'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'email' => 'Email',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsetInstMaintenances()
    {
        return $this->hasMany(\common\models\AsetInstMaintenance::className(), ['vendor_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsetInstOrders()
    {
        return $this->hasMany(\common\models\AsetInstOrder::className(), ['vendor_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsetWarranties()
    {
        return $this->hasMany(\common\models\AsetWarranty::className(), ['vendor_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\VendorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\VendorQuery(get_called_class());
    }
}
