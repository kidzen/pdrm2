<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inv_order".
 *
 * @property integer $id
 * @property integer $transaction_id
 * @property integer $arahan_kerja_id
 * @property string $order_date
 * @property string $ordered_by
 * @property string $order_no
 * @property string $required_date
 * @property string $checkout_date
 * @property integer $checkout_by
 * @property integer $approved
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\InvOrderItem[] $invOrderItems
 * @property \common\models\InvPackage[] $invPackages
 * @property \common\models\InvPlacement[] $invPlacements
 */
class InvOrder extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transaction_id', 'arahan_kerja_id', 'checkout_by', 'approved', 'approved_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['order_date', 'required_date', 'checkout_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['ordered_by', 'order_no'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_order';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_id' => 'Transaction ID',
            'arahan_kerja_id' => 'Arahan Kerja ID',
            'order_date' => 'Order Date',
            'ordered_by' => 'Ordered By',
            'order_no' => 'Order No',
            'required_date' => 'Required Date',
            'checkout_date' => 'Checkout Date',
            'checkout_by' => 'Checkout By',
            'approved' => 'Approved',
            'approved_at' => 'Approved At',
            'approved_by' => 'Approved By',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvOrderItems()
    {
        return $this->hasMany(\common\models\InvOrderItem::className(), ['order_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvPackages()
    {
        return $this->hasMany(\common\models\InvPackage::className(), ['order_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvPlacements()
    {
        return $this->hasMany(\common\models\InvPlacement::className(), ['order_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\InvOrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\InvOrderQuery(get_called_class());
    }
}
