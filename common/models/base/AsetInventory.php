<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "aset_inventory".
 *
 * @property integer $id
 * @property integer $subcategory_id
 * @property string $name
 * @property string $description
 * @property integer $type
 * @property string $remark
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \common\models\AsetInstOrderItem[] $asetInstOrderItems
 * @property \common\models\AsetSubcategory $subcategory
 * @property \common\models\AsetInventoryItem[] $asetInventoryItems
 */
class AsetInventory extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subcategory_id', 'type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'description', 'remark'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aset_inventory';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subcategory_id' => 'Subcategory ID',
            'name' => 'Name',
            'description' => 'Description',
            'type' => 'Type',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsetInstOrderItems()
    {
        return $this->hasMany(\common\models\AsetInstOrderItem::className(), ['inventory_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(\common\models\AsetSubcategory::className(), ['id' => 'subcategory_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsetInventoryItems()
    {
        return $this->hasMany(\common\models\AsetInventoryItem::className(), ['inventory_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AsetInventoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AsetInventoryQuery(get_called_class());
    }
}
