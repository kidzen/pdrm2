<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inv_transaction".
 *
 * @property integer $id
 * @property integer $type
 * @property string $check_date
 * @property integer $check_by
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\InvInventoryCheckin[] $invInventoryCheckins
 */
class InvTransaction extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'check_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['check_date', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_transaction';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'check_date' => 'Check Date',
            'check_by' => 'Check By',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvInventoryCheckins()
    {
        return $this->hasMany(\common\models\InvInventoryCheckin::className(), ['transaction_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\InvTransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\InvTransactionQuery(get_called_class());
    }
}
