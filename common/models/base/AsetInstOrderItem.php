<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "aset_inst_order_item".
 *
 * @property integer $id
 * @property integer $inst_order_id
 * @property integer $inventory_id
 * @property string $delivery_no
 * @property string $received_date
 * @property integer $quantity_request
 * @property integer $quantity_received
 * @property double $price_per_item
 * @property string $remark
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \common\models\AsetInstOrder $instOrder
 * @property \common\models\AsetInventory $inventory
 */
class AsetInstOrderItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inst_order_id', 'inventory_id', 'quantity_request', 'quantity_received', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['price_per_item'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['delivery_no', 'received_date', 'remark'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aset_inst_order_item';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inst_order_id' => 'Inst Order ID',
            'inventory_id' => 'Inventory ID',
            'delivery_no' => 'Delivery No',
            'received_date' => 'Received Date',
            'quantity_request' => 'Quantity Request',
            'quantity_received' => 'Quantity Received',
            'price_per_item' => 'Price Per Item',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstOrder()
    {
        return $this->hasOne(\common\models\AsetInstOrder::className(), ['id' => 'inst_order_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(\common\models\AsetInventory::className(), ['id' => 'inventory_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AsetInstOrderItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AsetInstOrderItemQuery(get_called_class());
    }
}
