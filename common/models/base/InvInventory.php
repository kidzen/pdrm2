<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inv_inventory".
 *
 * @property integer $id
 * @property integer $subcategory_id
 * @property string $card_no
 * @property string $code_no
 * @property string $description
 * @property integer $quantity
 * @property integer $min_stock
 * @property string $location
 * @property integer $approved
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\InvSubcategory $subcategory
 * @property \common\models\InvInventoryItem[] $invInventoryItems
 * @property \common\models\InvOrderItem[] $invOrderItems
 */
class InvInventory extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subcategory_id', 'quantity', 'min_stock', 'approved', 'approved_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['card_no', 'code_no', 'description', 'location'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_inventory';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subcategory_id' => 'Subcategory ID',
            'card_no' => 'Card No',
            'code_no' => 'Code No',
            'description' => 'Description',
            'quantity' => 'Quantity',
            'min_stock' => 'Min Stock',
            'location' => 'Location',
            'approved' => 'Approved',
            'approved_at' => 'Approved At',
            'approved_by' => 'Approved By',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(\common\models\InvSubcategory::className(), ['id' => 'subcategory_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvInventoryItems()
    {
        return $this->hasMany(\common\models\InvInventoryItem::className(), ['inventory_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvOrderItems()
    {
        return $this->hasMany(\common\models\InvOrderItem::className(), ['inventory_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\InvInventoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\InvInventoryQuery(get_called_class());
    }
}
