<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "aset_inventory_item".
 *
 * @property integer $id
 * @property integer $inventory_id
 * @property integer $parent_id
 * @property string $serial_no
 * @property string $model
 * @property string $cost
 * @property integer $warranty_id
 * @property integer $asset_class
 * @property integer $type
 * @property integer $engine_no
 * @property integer $casis_no
 * @property string $manufacture_no
 * @property string $reg_no
 * @property string $date_received
 * @property string $order_no_reff
 * @property string $order_date_reff
 * @property string $remark
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \common\models\AsetInstMaintenance[] $asetInstMaintenances
 * @property \common\models\AsetInventory $inventory
 * @property \common\models\AsetWarranty $warranty
 * @property \common\models\AsetItemMovement[] $asetItemMovements
 * @property \common\models\AsetItemPlacement[] $asetItemPlacements
 */
class AsetInventoryItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id', 'parent_id', 'warranty_id', 'asset_class', 'type', 'engine_no', 'casis_no', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['date_received', 'order_date_reff', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['serial_no', 'model', 'cost', 'manufacture_no', 'reg_no', 'order_no_reff', 'remark'], 'string', 'max' => 255],
            [['serial_no'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aset_inventory_item';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventory_id' => 'Inventory ID',
            'parent_id' => 'Parent ID',
            'serial_no' => 'Serial No',
            'model' => 'Model',
            'cost' => 'Cost',
            'warranty_id' => 'Warranty ID',
            'asset_class' => 'Asset Class',
            'type' => 'Type',
            'engine_no' => 'Engine No',
            'casis_no' => 'Casis No',
            'manufacture_no' => 'Manufacture No',
            'reg_no' => 'Reg No',
            'date_received' => 'Date Received',
            'order_no_reff' => 'Order No Reff',
            'order_date_reff' => 'Order Date Reff',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsetInstMaintenances()
    {
        return $this->hasMany(\common\models\AsetInstMaintenance::className(), ['item_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(\common\models\AsetInventory::className(), ['id' => 'inventory_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarranty()
    {
        return $this->hasOne(\common\models\AsetWarranty::className(), ['id' => 'warranty_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsetItemMovements()
    {
        return $this->hasMany(\common\models\AsetItemMovement::className(), ['item_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsetItemPlacements()
    {
        return $this->hasMany(\common\models\AsetItemPlacement::className(), ['item_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AsetInventoryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AsetInventoryItemQuery(get_called_class());
    }
}
