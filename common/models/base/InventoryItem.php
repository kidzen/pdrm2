<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inventory_item".
 *
 * @property integer $id
 * @property integer $inventory_id
 * @property integer $parent_id
 * @property string $serial_no
 * @property string $manufacture_no
 * @property string $cost
 * @property integer $warranty_id
 * @property integer $type
 * @property string $remark
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \common\models\InstMaintenance[] $instMaintenances
 * @property \common\models\Inventory $inventory
 * @property \common\models\Warranty $warranty
 * @property \common\models\ItemMovement[] $itemMovements
 * @property \common\models\ItemPlacement[] $itemPlacements
 */
class InventoryItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id', 'parent_id', 'warranty_id', 'type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['serial_no', 'manufacture_no', 'cost', 'remark'], 'string', 'max' => 255],
            [['serial_no'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory_item';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventory_id' => 'Inventory ID',
            'parent_id' => 'Parent ID',
            'serial_no' => 'Serial No',
            'manufacture_no' => 'Manufacture No',
            'cost' => 'Cost',
            'warranty_id' => 'Warranty ID',
            'type' => 'Type',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstMaintenances()
    {
        return $this->hasMany(\common\models\InstMaintenance::className(), ['item_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(\common\models\Inventory::className(), ['id' => 'inventory_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarranty()
    {
        return $this->hasOne(\common\models\Warranty::className(), ['id' => 'warranty_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemMovements()
    {
        return $this->hasMany(\common\models\ItemMovement::className(), ['item_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemPlacements()
    {
        return $this->hasMany(\common\models\ItemPlacement::className(), ['item_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\InventoryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\InventoryItemQuery(get_called_class());
    }
}
