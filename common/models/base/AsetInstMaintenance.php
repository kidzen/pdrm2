<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "aset_inst_maintenance".
 *
 * @property integer $id
 * @property integer $t_id
 * @property string $inst_no
 * @property integer $vendor_id
 * @property integer $item_id
 * @property integer $type
 * @property integer $total_cost
 * @property integer $approval
 * @property integer $approved_by
 * @property string $approved_at
 * @property integer $received_by
 * @property string $received_at
 * @property integer $requested_by
 * @property string $requested_at
 * @property string $remark
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \common\models\AsetInventoryItem $item
 * @property \common\models\AsetTransaction $t
 * @property \common\models\Vendor $vendor
 */
class AsetInstMaintenance extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['t_id', 'vendor_id', 'item_id', 'type', 'total_cost', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['inst_no', 'remark'], 'string', 'max' => 255],
            [['inst_no'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aset_inst_maintenance';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            't_id' => 'T ID',
            'inst_no' => 'Inst No',
            'vendor_id' => 'Vendor ID',
            'item_id' => 'Item ID',
            'type' => 'Type',
            'total_cost' => 'Total Cost',
            'approval' => 'Approval',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'received_by' => 'Received By',
            'received_at' => 'Received At',
            'requested_by' => 'Requested By',
            'requested_at' => 'Requested At',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(\common\models\AsetInventoryItem::className(), ['id' => 'item_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getT()
    {
        return $this->hasOne(\common\models\AsetTransaction::className(), ['id' => 't_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(\common\models\Vendor::className(), ['id' => 'vendor_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AsetInstMaintenanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AsetInstMaintenanceQuery(get_called_class());
    }
}
