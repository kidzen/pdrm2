<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inv_inventory_item".
 *
 * @property integer $id
 * @property integer $inventory_id
 * @property integer $checkin_id
 * @property integer $checkout_id
 * @property string $sku
 * @property string $unit_price
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\InvInventory $inventory
 * @property \common\models\InvInventoryCheckin $checkin
 * @property \common\models\InvOrderItem $checkout
 */
class InvInventoryItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id', 'checkin_id', 'checkout_id', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['sku'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_inventory_item';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventory_id' => 'Inventory ID',
            'checkin_id' => 'Checkin ID',
            'checkout_id' => 'Checkout ID',
            'sku' => 'Sku',
            'unit_price' => 'Unit Price',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(\common\models\InvInventory::className(), ['id' => 'inventory_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckin()
    {
        return $this->hasOne(\common\models\InvInventoryCheckin::className(), ['id' => 'checkin_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckout()
    {
        return $this->hasOne(\common\models\InvOrderItem::className(), ['id' => 'checkout_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\InvInventoryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\InvInventoryItemQuery(get_called_class());
    }
}
