<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inv_order_item".
 *
 * @property integer $id
 * @property integer $inventory_id
 * @property integer $order_id
 * @property integer $rq_quantity
 * @property integer $app_quantity
 * @property integer $current_balance
 * @property double $unit_price
 * @property string $string
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\InvInventoryItem[] $invInventoryItems
 * @property \common\models\InvOrder $order
 * @property \common\models\InvInventory $inventory
 */
class InvOrderItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id', 'order_id', 'rq_quantity', 'app_quantity', 'current_balance', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['string'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_order_item';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventory_id' => 'Inventory ID',
            'order_id' => 'Order ID',
            'rq_quantity' => 'Rq Quantity',
            'app_quantity' => 'App Quantity',
            'current_balance' => 'Current Balance',
            'unit_price' => 'Unit Price',
            'string' => 'String',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvInventoryItems()
    {
        return $this->hasMany(\common\models\InvInventoryItem::className(), ['checkout_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(\common\models\InvOrder::className(), ['id' => 'order_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(\common\models\InvInventory::className(), ['id' => 'inventory_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\InvOrderItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\InvOrderItemQuery(get_called_class());
    }
}
