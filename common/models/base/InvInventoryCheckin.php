<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inv_inventory_checkin".
 *
 * @property integer $id
 * @property integer $transaction_id
 * @property integer $inventory_id
 * @property integer $vendor_id
 * @property integer $items_quantity
 * @property double $items_total_price
 * @property string $check_date
 * @property integer $check_by
 * @property integer $approved
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\InvTransaction $transaction
 * @property \common\models\InvInventoryItem[] $invInventoryItems
 */
class InvInventoryCheckin extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transaction_id', 'inventory_id', 'vendor_id', 'items_quantity', 'check_by', 'approved', 'approved_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['items_total_price'], 'number'],
            [['check_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_inventory_checkin';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_id' => 'Transaction ID',
            'inventory_id' => 'Inventory ID',
            'vendor_id' => 'Vendor ID',
            'items_quantity' => 'Items Quantity',
            'items_total_price' => 'Items Total Price',
            'check_date' => 'Check Date',
            'check_by' => 'Check By',
            'approved' => 'Approved',
            'approved_at' => 'Approved At',
            'approved_by' => 'Approved By',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(\common\models\InvTransaction::className(), ['id' => 'transaction_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvInventoryItems()
    {
        return $this->hasMany(\common\models\InvInventoryItem::className(), ['checkin_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\InvInventoryCheckinQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\InvInventoryCheckinQuery(get_called_class());
    }
}
