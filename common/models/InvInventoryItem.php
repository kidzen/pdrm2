<?php

namespace common\models;

use \common\models\base\InvInventoryItem as BaseInvInventoryItem;

/**
 * This is the model class for table "inv_inventory_item".
 */
class InvInventoryItem extends BaseInvInventoryItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['inventory_id', 'checkin_id', 'checkout_id', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['sku'], 'string', 'max' => 255]
        ]);
    }
	
}
