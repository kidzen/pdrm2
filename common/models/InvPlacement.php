<?php

namespace common\models;

use \common\models\base\InvPlacement as BaseInvPlacement;

/**
 * This is the model class for table "inv_placement".
 */
class InvPlacement extends BaseInvPlacement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type', 'order_id', 'store_id', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
}
