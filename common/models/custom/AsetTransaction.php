<?php

namespace common\models\custom;

use \common\models\base\AsetTransaction as BaseAsetTransaction;

/**
 * This is the model class for table "aset_transaction".
 */
class AsetTransaction extends BaseAsetTransaction
{
    /**
     * @inheritdoc
     */
    const TYPE_REGISTRATION = 0;
    const TYPE_APPROVAL = 1;
    const TYPE_REJECTION = 2;
    const TYPE_DISPOSAL = 3;
}
