<?php

namespace common\models\custom;

use \common\models\base\AsetInstOrder as BaseAsetInstOrder;
use \common\models\AsetTransaction;

/**
 * This is the model class for table "aset_inst_order".
 */
class AsetInstOrder extends BaseAsetInstOrder
{
    /**
     * @inheritdoc
     */

    public function generateInstNo() {
        if(\Yii::$app->db->driverName == 'oci'){
            $data = static::find()->max("CAST(SUBSTR(inst_no,19) AS UNSIGNED)");
        } else if(\Yii::$app->db->driverName == 'mysql'){
            $data = static::find()->max("SUBSTR(inst_no,18)");
        }
        $bil = $data + 1;
        $bilNo = 'PDRM/ASSET/'.date('y/m/').$bil;
        return $bilNo;

    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->inst_no = $this->generateInstNo();
            $trans = new AsetTransaction();
            $trans->type = $trans::TYPE_REGISTRATION;
            $trans->remark = 'Asset Registration';
            $trans->save();
            // if($trans->save() && !empty()){

                return true;
            // }
        } else {
            return false;
        }
    }

}
