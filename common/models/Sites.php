<?php

namespace common\models;

use \common\models\base\Sites as BaseSites;

/**
 * This is the model class for table "sites".
 */
class Sites extends BaseSites
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'description', 'remark'], 'string', 'max' => 255],
            [['name'], 'unique']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
}
