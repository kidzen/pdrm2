<?php

namespace common\models;

use \common\models\base\AsetWarranty as BaseAsetWarranty;

/**
 * This is the model class for table "aset_warranty".
 */
class AsetWarranty extends BaseAsetWarranty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['vendor_id', 'type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['remark'], 'string', 'max' => 255]
        ]);
    }
	
}
