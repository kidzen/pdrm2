<?php

namespace common\models;

use \common\models\base\InvStoreList as BaseInvStoreList;

/**
 * This is the model class for table "inv_store_list".
 */
class InvStoreList extends BaseInvStoreList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['deleted', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'location'], 'string', 'max' => 255]
        ]);
    }
	
}
