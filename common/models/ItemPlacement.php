<?php

namespace common\models;

use \common\models\base\ItemPlacement as BaseItemPlacement;

/**
 * This is the model class for table "item_placement".
 */
class ItemPlacement extends BaseItemPlacement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['item_id', 'item_movement_id', 'site_id', 'type', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['remark'], 'string', 'max' => 255]
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'item_movement_id' => 'Item Movement ID',
            'site_id' => 'Site ID',
            'type' => 'Type',
            'approval' => 'Approval',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'received_by' => 'Received By',
            'received_at' => 'Received At',
            'requested_by' => 'Requested By',
            'requested_at' => 'Requested At',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
}
