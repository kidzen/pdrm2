<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AsetInventoryItem;

/**
 * common\models\search\AsetInventoryItemSearch represents the model behind the search form about `common\models\AsetInventoryItem`.
 */
 class AsetInventoryItemSearch extends AsetInventoryItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inventory_id', 'parent_id', 'warranty_id', 'asset_class', 'type', 'engine_no', 'casis_no', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['serial_no', 'model', 'cost', 'manufacture_no', 'reg_no', 'date_received', 'order_no_reff', 'order_date_reff', 'remark', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AsetInventoryItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'inventory_id' => $this->inventory_id,
            'parent_id' => $this->parent_id,
            'warranty_id' => $this->warranty_id,
            'asset_class' => $this->asset_class,
            'type' => $this->type,
            'engine_no' => $this->engine_no,
            'casis_no' => $this->casis_no,
            'date_received' => $this->date_received,
            'order_date_reff' => $this->order_date_reff,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'serial_no', $this->serial_no])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'cost', $this->cost])
            ->andFilterWhere(['like', 'manufacture_no', $this->manufacture_no])
            ->andFilterWhere(['like', 'reg_no', $this->reg_no])
            ->andFilterWhere(['like', 'order_no_reff', $this->order_no_reff])
            ->andFilterWhere(['like', 'remark', $this->remark]);

        return $dataProvider;
    }
}
