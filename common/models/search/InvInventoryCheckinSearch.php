<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InvInventoryCheckin;

/**
 * common\models\search\InvInventoryCheckinSearch represents the model behind the search form about `common\models\InvInventoryCheckin`.
 */
 class InvInventoryCheckinSearch extends InvInventoryCheckin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'inventory_id', 'vendor_id', 'items_quantity', 'check_by', 'approved', 'approved_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['items_total_price'], 'number'],
            [['check_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvInventoryCheckin::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'inventory_id' => $this->inventory_id,
            'vendor_id' => $this->vendor_id,
            'items_quantity' => $this->items_quantity,
            'items_total_price' => $this->items_total_price,
            'check_date' => $this->check_date,
            'check_by' => $this->check_by,
            'approved' => $this->approved,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
