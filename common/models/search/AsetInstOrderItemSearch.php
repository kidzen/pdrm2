<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AsetInstOrderItem;

/**
 * common\models\search\AsetInstOrderItemSearch represents the model behind the search form about `common\models\AsetInstOrderItem`.
 */
 class AsetInstOrderItemSearch extends AsetInstOrderItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inst_order_id', 'inventory_id', 'quantity_request', 'quantity_received', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['delivery_no', 'received_date', 'remark', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['price_per_item'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AsetInstOrderItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'inst_order_id' => $this->inst_order_id,
            'inventory_id' => $this->inventory_id,
            'quantity_request' => $this->quantity_request,
            'quantity_received' => $this->quantity_received,
            'price_per_item' => $this->price_per_item,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'delivery_no', $this->delivery_no])
            ->andFilterWhere(['like', 'received_date', $this->received_date])
            ->andFilterWhere(['like', 'remark', $this->remark]);

        return $dataProvider;
    }
}
