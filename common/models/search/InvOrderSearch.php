<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InvOrder;

/**
 * common\models\search\InvOrderSearch represents the model behind the search form about `common\models\InvOrder`.
 */
 class InvOrderSearch extends InvOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'arahan_kerja_id', 'checkout_by', 'approved', 'approved_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['order_date', 'ordered_by', 'order_no', 'required_date', 'checkout_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvOrder::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'arahan_kerja_id' => $this->arahan_kerja_id,
            'order_date' => $this->order_date,
            'required_date' => $this->required_date,
            'checkout_date' => $this->checkout_date,
            'checkout_by' => $this->checkout_by,
            'approved' => $this->approved,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'ordered_by', $this->ordered_by])
            ->andFilterWhere(['like', 'order_no', $this->order_no]);

        return $dataProvider;
    }
}
