<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AsetInstMaintenance;

/**
 * common\models\search\AsetInstMaintenanceSearch represents the model behind the search form about `common\models\AsetInstMaintenance`.
 */
 class AsetInstMaintenanceSearch extends AsetInstMaintenance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 't_id', 'vendor_id', 'item_id', 'type', 'total_cost', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['inst_no', 'approved_at', 'received_at', 'requested_at', 'remark', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AsetInstMaintenance::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            't_id' => $this->t_id,
            'vendor_id' => $this->vendor_id,
            'item_id' => $this->item_id,
            'type' => $this->type,
            'total_cost' => $this->total_cost,
            'approval' => $this->approval,
            'approved_by' => $this->approved_by,
            'approved_at' => $this->approved_at,
            'received_by' => $this->received_by,
            'received_at' => $this->received_at,
            'requested_by' => $this->requested_by,
            'requested_at' => $this->requested_at,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'inst_no', $this->inst_no])
            ->andFilterWhere(['like', 'remark', $this->remark]);

        return $dataProvider;
    }
}
