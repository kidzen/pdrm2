<?php

namespace common\models;

use \common\models\base\AsetInventoryPlacement as BaseAsetInventoryPlacement;

/**
 * This is the model class for table "aset_inventory_placement".
 */
class AsetInventoryPlacement extends BaseAsetInventoryPlacement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['asset_id', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['location', 'remark'], 'string', 'max' => 255],
            [['location'], 'unique']
        ]);
    }
	
}
