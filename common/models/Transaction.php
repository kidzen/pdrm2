<?php

namespace common\models;

use \common\models\base\Transaction as BaseTransaction;

/**
 * This is the model class for table "transaction".
 */
class Transaction extends BaseTransaction
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    const TYPE_PURCHASING = 1;
    const TYPE_MAINTENANCE = 1;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['transaction_no', 'remark'], 'string', 'max' => 255],
            [['transaction_no'], 'unique']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'transaction_no' => 'Transaction No',
            'type' => 'Type',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }

    public function generateTransNo() {
        $data = static::find()->max("CAST(SUBSTR(transaction_no,19) AS UNSIGNED)");
        $bil = $data + 1;
        $bilPrefix = 'PDRM/TRANS/'.date('Y/m/');
        return $bilPrefix.$bil;

    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->transaction_no = $this->generateTransNo();
            return true;
        } else {
            return false;
        }
    }
}
