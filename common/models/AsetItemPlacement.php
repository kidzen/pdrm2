<?php

namespace common\models;

use \common\models\base\AsetItemPlacement as BaseAsetItemPlacement;

/**
 * This is the model class for table "aset_item_placement".
 */
class AsetItemPlacement extends BaseAsetItemPlacement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['item_id', 'item_movement_id', 'site_id', 'type', 'approval', 'approved_by', 'received_by', 'requested_by', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['approved_at', 'received_at', 'requested_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['remark'], 'string', 'max' => 255]
        ]);
    }
	
}
