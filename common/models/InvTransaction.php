<?php

namespace common\models;

use \common\models\base\InvTransaction as BaseInvTransaction;

/**
 * This is the model class for table "inv_transaction".
 */
class InvTransaction extends BaseInvTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type', 'check_by', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['check_date', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
}
